import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FusionChartComponent } from './fusion-chart/fusion-chart.component';


const routes: Routes = [
  {
    path: 'fusion-chart',
    component: FusionChartComponent,
    data: { title: 'CHART' }
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
