import { Component, OnInit } from '@angular/core';

import { dummyData } from '../dummy-data';
import * as _ from 'lodash';
import * as FusionCharts from "fusioncharts";
import { from } from 'rxjs';
import { DatePipe } from '@angular/common';
@Component({
  selector: 'app-fusion-chart',
  templateUrl: './fusion-chart.component.html',
  styleUrls: ['./fusion-chart.component.scss']
})
export class FusionChartComponent {

  width = '100%';
  height = '400';
  type = "timeseries";
  dataFormat = "json";

  dataSource: any;
  chartObj: any;
  chartData = [];
  chartSchema: any;

  essayCallResponse = [];
  grammerCallResponse = [];
  plagiarismCallsResponse = [];
  shortAnswerCallsResponse = [];
  essayWithTutorCallsResponse = [];
  shortAnswerWithTutorCallsResponse = [];

  constructor(private datePipe: DatePipe) { 

    this.chartObj = dummyData['serviceDTOWithDate'];

    this.chartSchema = [{
      "name": 'Time',
      "type": "date",
      "format": "%d-%b-%y"
    }, {
      "name": 'ApiCallName',
      "type": "string"
    }, {
      "name": 'ApiCount',
      "type": "number"
    }];
    this.dataSource =  {
      chart: {},
      caption: {
      },
      subcaption: {
      },
      series: "ApiCallName",
      yaxis: [
        {
          plot: "Days of the Month",
          title: "# of Documents Checked",           
        }
      ],
    };
    
    this.chartDataConversion();
    this.fetchData();

    // this.type = "timeseries";
    // this.width = "100%";
    // this.height = "400";

    // // This is the dataSource of the chart
    // this.dataSource = {
    //   chart: {},
    //   caption: {
    //     text: "Sales Analysis"
    //   },
    //   subcaption: {
    //     text: "Grocery & Footwear"
    //   },
    //   series: "Type",
    //   yaxis: [
    //     {
    //       plot: "Sales Value",
    //       title: "Sale Value",
    //       format: {
    //         prefix: "$"
    //       }
    //     }
    //   ]
    // };

    //this.fetchData1();
  }
  fetchData () {
    const fusionDataStore = new FusionCharts.DataStore();    
    const fusionTable = fusionDataStore.createDataTable(this.chartData, this.chartSchema);    
    this.dataSource.data = fusionTable;
  } // FN

  chartDataConversion () {
    
    // Extracts the required keys for chart creation
    const chartDataInput  = _.pick(this.chartObj,Object.keys({
      'essayCalls':  'Essay Calls',
      'grammerCalls': 'Grammar Calls',
      'plagiarismCalls': 'Plagiarism Calls',
      'shortAnswerCalls': 'Short Answer Calls',
      'essayWithTutorCalls': 'Essay Calls with Tutor',
      'shortAnswerWithTutorCalls': 'Short Answer Calls with Tutor'
    }));
    const chartDataInputValuesLength = Object.values(chartDataInput).map((x:Array<number>) => x.length);
    let maxApiCallVal= Math.max(...chartDataInputValuesLength);

    // Segregate Service details according to the Api Calls
    for(let i=0; i < maxApiCallVal; i++) {
      this.essayCallResponse.push({
        Time: this.datePipe.transform(this.chartObj.essayCallsUpdt[i],"dd-MMM-yy"),
        ApiCallName: 'Essay Calls',
        ApiCount: this.chartObj.essayCalls[i]
      });

      this.grammerCallResponse.push({
        Time: this.datePipe.transform(this.chartObj.grammerCallsUpdt[i],"dd-MMM-yy"),
        ApiCallName: 'Grammar Calls',
        ApiCount: this.chartObj.grammerCalls[i]
      });

      this.plagiarismCallsResponse.push({
        Time: this.datePipe.transform(this.chartObj.plagiarismCallsUpdt[i],"dd-MMM-yy"),
        ApiCallName: 'Plagiarism Calls',
        ApiCount: this.chartObj.plagiarismCalls[i]
      });

      this.shortAnswerCallsResponse.push({
        Time: this.datePipe.transform(this.chartObj.shortAnswerCallsUpdt[i],"dd-MMM-yy"),
        ApiCallName: 'Short Answer Calls',
        ApiCount: this.chartObj.shortAnswerCalls[i]
      });

      this.essayWithTutorCallsResponse.push({
        Time: this.datePipe.transform(this.chartObj.essayWithTutorCallsUpdt[i],"dd-MMM-yy"),
        ApiCallName: 'Essay Calls with Tutor',
        ApiCount: this.chartObj.essayWithTutorCalls[i]
      });

      this.shortAnswerWithTutorCallsResponse.push({
        Time: this.datePipe.transform(this.chartObj.shortAnswerWithTutorCallsUpdt[i],"dd-MMM-yy"),
        ApiCallName: 'Short Answer Calls with Tutor',
        ApiCount: this.chartObj.shortAnswerWithTutorCalls[i]
      });
    } // FOR

    // Insert Values into chart in order to map with Graph
    for ( let a=0; a < maxApiCallVal; a++ ) {
      for ( let x=a*6; x < maxApiCallVal*6; x++ ) {
        if ( x%6 === 0) {
          this.chartData[x] = Object.values(this.essayCallResponse[a]);
        } else if ( (x-1)%6 === 0 ) {
          this.chartData[x] = Object.values(this.grammerCallResponse[a]);
        } else if( (x-2)%6 === 0 ) {
          this.chartData[x] = Object.values(this.plagiarismCallsResponse[a]);
        } else if( (x-3)%6 === 0 ) {
          this.chartData[x] = Object.values(this.shortAnswerCallsResponse[a]);
        } else if( (x-4)%6 === 0 ) {
          this.chartData[x] = Object.values(this.essayWithTutorCallsResponse[a]);
        } else {
          this.chartData[x] = Object.values(this.shortAnswerWithTutorCallsResponse[a]);
        }
      } // INNER FOR
    } // FOR
  } // FN


  // In this method we will create our DataStore and using that we will create a custom DataTable which takes two
  // parameters, one is data another is schema.
  // fetchData1() {
  //   var jsonify = res => res.json();
  //   var dataFetch = fetch(
  //     "https://s3.eu-central-1.amazonaws.com/fusion.store/ft/data/plotting-multiple-series-on-time-axis-data.json"
  //   ).then(jsonify);
  //   var schemaFetch = fetch(
  //     "https://s3.eu-central-1.amazonaws.com/fusion.store/ft/schema/plotting-multiple-series-on-time-axis-schema.json"
  //   ).then(jsonify);

  //   Promise.all([dataFetch, schemaFetch]).then(res => {
  //     const [data, schema] = res;
  //     // First we are creating a DataStore
  //     const fusionDataStore = new FusionCharts.DataStore();
  //     // After that we are creating a DataTable by passing our data and schema as arguments
  //     const fusionTable = fusionDataStore.createDataTable(data, schema);
  //     // Afet that we simply mutated our timeseries datasource by attaching the above
  //     // DataTable into its data property.
  //     this.dataSource.data = fusionTable;
  //   });
  // }

}
