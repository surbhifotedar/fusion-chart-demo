import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { FusionChartsModule } from 'angular-fusioncharts';
import * as FusionCharts from 'fusioncharts';
import * as FusionTheme from 'fusioncharts/themes/fusioncharts.theme.fusion';
import * as Charts from 'fusioncharts/fusioncharts.charts';

import { FusionChartComponent } from './fusion-chart/fusion-chart.component';
import { DatePipe, APP_BASE_HREF } from '@angular/common';

FusionChartsModule.fcRoot(FusionCharts, Charts, FusionTheme);

@NgModule({
  declarations: [
    AppComponent,
    FusionChartComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FusionChartsModule
  ],
  providers: [DatePipe,
  {provide: APP_BASE_HREF, useValue: '/'}],
  bootstrap: [AppComponent]
})
export class AppModule { }
